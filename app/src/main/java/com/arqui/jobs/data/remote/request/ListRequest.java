package com.arqui.jobs.data.remote.request;

import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.entities.UserByJob;
import com.arqui.jobs.data.entities.trackholder.TrackEntityHolder;
import com.arqui.jobs.data.entities.trackholder.TrackEntityHolderUser;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by katherine on 12/06/17.
 */

public interface ListRequest {

    @GET("jobs")
    Call<TrackEntityHolder<JobEntity>> getJobs();


    @GET("admin/{id}/jobs")
    Call<TrackEntityHolder<JobEntity>> getAdminJobs(@Path("id") int idAdmin);

    @GET("admin/jobs/{id}/applicationoffer")
    Call<TrackEntityHolderUser<UserByJob>> getUsersByJobs(@Path("id") int idJob);

}
