package com.arqui.jobs.data.entities;

import java.io.Serializable;

/**
 * Created by junior on 30/09/16.
 */
public class UserEntity implements Serializable {

    private String userEmail;
    private String userLastName1;
    private String userLastName2;
    private String userName;
    private String password;
    private String userPhone;
    private int userType;
    private int idUser;

    public UserEntity(String email, String last_name, String first_name, String password, String phone, int userType) {
        this.userEmail = email;
        this.userLastName1 = last_name;
        this.userName = first_name;
        this.password = password;
        this.userPhone = phone;
        this.userType = userType;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserLastName2() {
        return userLastName2;
    }

    public void setUserLastName2(String userLastName2) {
        this.userLastName2 = userLastName2;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserLastName() {
        return userLastName1;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName1 = userLastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getFullName(){

        return userName + " " + userLastName1;
    }
}
