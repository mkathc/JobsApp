package com.arqui.jobs.data.entities;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kath on 2/06/18.
 */

public class JobEntity implements Serializable {

    private int idJobOffer;
    private String JobOfferTitle;
    private String JobOfferDescription;
    private String JobOfferExpiration;
    private int User_idUser;

    public int getIdJobOffer() {
        return idJobOffer;
    }

    public void setIdJobOffer(int idJobOffer) {
        this.idJobOffer = idJobOffer;
    }

    public int getUser_idUser() {
        return User_idUser;
    }

    public void setUser_idUser(int user_idUser) {
        User_idUser = user_idUser;
    }

    public String getJobOfferTitle() {
        return JobOfferTitle;
    }

    public void setJobOfferTitle(String jobOfferTitle) {
        JobOfferTitle = jobOfferTitle;
    }

    public String getJobOfferDescription() {
        return JobOfferDescription;
    }

    public void setJobOfferDescription(String jobOfferDescription) {
        JobOfferDescription = jobOfferDescription;
    }

    public String getJobOfferExpiration() {
        return JobOfferExpiration;
    }

    public void setJobOfferExpiration(String jobOfferExpiration) {
        JobOfferExpiration = jobOfferExpiration;
    }

    public String getExpiration(){
        if (getJobOfferExpiration() == null ){
            return "";
        }
        Date tempDate = null;
        SimpleDateFormat parseDateFromServer= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat parseDateForShowDetail =  new SimpleDateFormat("dd' de 'MMMM' del 'yyyy", new Locale("es","ES"));

        try {
            tempDate = parseDateFromServer.parse(getJobOfferExpiration());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parseDateForShowDetail.format(tempDate);
    }
}
