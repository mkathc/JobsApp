package com.arqui.jobs.presentation.main.jobs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arqui.jobs.R;
import com.arqui.jobs.core.LoaderAdapter;
import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.local.SessionManager;
import com.arqui.jobs.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by katherine on 31/05/17.
 */

public class JobsAdapter extends LoaderAdapter<JobEntity> implements OnClickListListener {


    private Context context;
    private JobItem jobItem;
    private SessionManager mSessionManager;

    public JobsAdapter(ArrayList<JobEntity> jobEntities, Context context, JobItem jobItem) {
        super(context);
        setItems(jobEntities);
        this.context = context;
        this.jobItem = jobItem;
        mSessionManager = new SessionManager(context);
    }

    public JobsAdapter(ArrayList<JobEntity> jobEntities, Context context) {
        super(context);
        setItems(jobEntities);
        this.context = context;

    }

    public ArrayList<JobEntity> getItems() {
        return (ArrayList<JobEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jobs, parent, false);
        return new ViewHolder(root, this);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        JobEntity jobEntity = getItems().get(position);
        //((ViewHolder) holder).cardView.setBackgroundResource(ticketEntity.getSchedules().getDestiny().getImage_1());
        ((ViewHolder) holder).tvName.setText(jobEntity.getJobOfferTitle());
        ((ViewHolder) holder).tvDescript.setText(jobEntity.getJobOfferDescription());
        ((ViewHolder) holder).tvExpirate.setText(jobEntity.getExpiration());

        if (jobEntity.getUser_idUser() == mSessionManager.getUserEntity().getIdUser()) {
            ((ViewHolder) holder).tvAplicar.setText("Ver Candidatos");
        }
    }

    @Override
    public void onClick(int position) {
        JobEntity jobEntity = getItems().get(position);
        jobItem.clickItem(jobEntity);
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_descript)
        TextView tvDescript;
        @BindView(R.id.tv_expirate)
        TextView tvExpirate;
        @BindView(R.id.tv_aplicar)
        TextView tvAplicar;
        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
