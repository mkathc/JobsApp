package com.arqui.jobs.presentation.main.jobs;

import com.arqui.jobs.data.entities.JobEntity;

/**
 * Created by kath on 2/06/18.
 */

public interface JobItem {

    void clickItem(JobEntity jobEntity);

    void deleteItem(JobEntity jobEntity, int position);
}
