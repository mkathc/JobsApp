package com.arqui.jobs.presentation.main.user;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arqui.jobs.R;
import com.arqui.jobs.core.BaseActivity;
import com.arqui.jobs.core.BaseFragment;
import com.arqui.jobs.core.RecyclerViewScrollListener;
import com.arqui.jobs.core.ScrollChildSwipeRefreshLayout;
import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.local.SessionManager;
import com.arqui.jobs.presentation.main.jobs.JobItem;
import com.arqui.jobs.presentation.main.jobs.JobsAdapter;
import com.arqui.jobs.presentation.main.jobs.JobsContract;
import com.arqui.jobs.presentation.main.jobs.JobsPresenter;
import com.arqui.jobs.presentation.profile.ProfileActivity;
import com.arqui.jobs.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 31/05/17.
 */

public class UserFragment extends BaseFragment implements JobsContract.View {


    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.noListIcon)
    ImageView noListIcon;
    @BindView(R.id.noListMain)
    TextView noListMain;
    @BindView(R.id.noList)
    LinearLayout noList;
    @BindView(R.id.refresh_layout)
    ScrollChildSwipeRefreshLayout refreshLayout;
    Unbinder unbinder;
    @BindView(R.id.profile)
    FloatingActionButton profile;

    private JobsAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private JobsContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    private SessionManager mSessionManager;
    private AlertDialog dialogSend;

    public UserFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadList();

    }

    public static UserFragment newInstance() {
        return new UserFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        mPresenter = new JobsPresenter(this, getContext());
        setHasOptionsMenu(true);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.black),
                ContextCompat.getColor(getActivity(), R.color.dark_gray),
                ContextCompat.getColor(getActivity(), R.color.black)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(rvList);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //mPresenter.start();
                mPresenter.loadList();
            }
        });

        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Obteniendo datos...");
        mLayoutManager = new LinearLayoutManager(getContext());
        rvList.setLayoutManager(mLayoutManager);
        mAdapter = new JobsAdapter(new ArrayList<JobEntity>(), getContext(), (JobItem) mPresenter);
        rvList.setAdapter(mAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                //Toast.makeText(getActivity(), "Favoritos", Toast.LENGTH_SHORT).show();
                nextActivity(getActivity(), null, ProfileActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void getJobsList(ArrayList<JobEntity> list) {
        mAdapter.setItems(list);
        if (list != null) {
            noList.setVisibility((list.size() > 0) ? View.GONE : View.VISIBLE);
        }

        rvList.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                mPresenter.loadList();
            }
        });
    }

    @Override
    public void clickItemJob(JobEntity jobEntity) {

        sendDataForJob(jobEntity.getIdJobOffer());

    }

    @Override
    public void responseSendData(String message) {
        showMessage(message);
    }

    public void sendDataForJob(final int idJob) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Hola, " + mSessionManager.getUserEntity().getUserName());
        builder.setMessage("¿Deseas enviar tus datos a este trabajo?");
        builder.setCancelable(false);
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogSend.dismiss();
            }
        });
        builder.setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

                mPresenter.sendDatoForJob(idJob, mSessionManager.getUserEntity().getIdUser());

            }
        });
        dialogSend = builder.create();
        dialogSend.show();
    }


    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(JobsContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });

        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick(R.id.profile)
    public void onViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("myProfile", true);
        nextActivity(getActivity(), bundle, ProfileActivity.class, false);
    }
}
