package com.arqui.jobs.presentation.auth;

import android.content.Context;
import android.support.annotation.NonNull;

import com.arqui.jobs.data.entities.AccessTokenEntity;
import com.arqui.jobs.data.entities.UserEntity;
import com.arqui.jobs.data.local.SessionManager;
import com.arqui.jobs.data.remote.ServiceFactory;
import com.arqui.jobs.data.remote.request.LoginRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by katherine on 10/05/17.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View mView;
    private Context context;
    private final SessionManager mSessionManager;

    public LoginPresenter(@NonNull LoginContract.View mView, @NonNull Context context) {
        this.context = checkNotNull(context, "context cannot be null!");
        this.mView = checkNotNull(mView, "newsView cannot be null!");
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }

    @Override
    public void loginUser(String username, final String password, final int userType) {
        LoginRequest loginService =
                ServiceFactory.createService(LoginRequest.class);
        Call<AccessTokenEntity> call = loginService.login(username,password, userType);
        mView.setLoadingIndicator(true);
        call.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                if (response.isSuccessful()) {

                    switch (response.code()){
                        case 200:
                            mSessionManager.setUserType(userType);
                            openSession(response.body(), response.body().getUser());
                            break;
                        case 202:
                            mView.setLoadingIndicator(false);
                            mView.errorLogin("Verifica si tu cuenta es de administrador o usuario");
                            break;

                    }
                } else {
                    mView.setLoadingIndicator(false);
                    mView.errorLogin("Login Fallido");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.errorLogin("No se puede conectar al servidor");
            }
        });
    }



    @Override
    public void openSession(AccessTokenEntity token, UserEntity userEntity) {

        mSessionManager.openSession(token);
        mSessionManager.setUser(userEntity);
        mView.setLoadingIndicator(false);
        mView.loginSuccessful(userEntity);
    }

    @Override
    public void start() {

    }
}
