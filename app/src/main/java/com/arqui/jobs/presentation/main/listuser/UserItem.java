package com.arqui.jobs.presentation.main.listuser;

import com.arqui.jobs.data.entities.JobEntity;
import com.arqui.jobs.data.entities.UserEntity;

/**
 * Created by kath on 2/06/18.
 */

public interface UserItem {

    void clickItem(UserEntity userEntity);

}
