package com.arqui.jobs.presentation.profile;

import com.arqui.jobs.core.BasePresenter;
import com.arqui.jobs.core.BaseView;
import com.arqui.jobs.data.entities.UserEntity;

import java.io.File;

/**
 * Created by katherine on 21/06/17.
 */

public interface ProfileContract {
    interface View extends BaseView<Presenter> {
        void ShowSessionInformation(UserEntity userEntity);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

    }
}
